/* ####################################
IMPLICIT DATA STRUCTURES:
    VISITOR
      visitor_id <pk>
      visit_start_timestamp (referred to as "transaction_date" in output)
      
    TRANSACTION (referred to as a "visit" in the output, and in the SQL below)
      visitor_id <pk>
      visit_num <pk>
      transaction_type

    TRANSACTION_DETAIL
      visitor_id <pk,fk>
      visit_num <pk,fk>
      transaction_action <pk>  
      hit_timestamp

REQUIREMENT:
    count of unique visitor_id's, by date
    count of unique visitor_id's, by date / transaction_type
    count of unique visitor_id's, by date / transaction_type / transaction_action
    
    count of unique visits (transactions), by date 
    count of unique visits (transactions), by date / transaction_type 
    count of unique visits (transactions), by date / transaction_type / transaction_action
    
####################################
 */

USE evolytics;

/* Pivot the data to show one record for each "visit". */
WITH visits (visitor_id,visit_num,transaction_type,transaction_date,started_visitor_id,failed_visitor_id,completed_visitor_id) as (
  SELECT
    visitor_id,
    visit_num,
    transaction_type,
    CONVERT(visit_start_timestamp, DATE),
    MIN(CASE WHEN transaction_action='transaction_started' THEN visitor_id END) as started_visitor_id,
    MIN(CASE WHEN transaction_action='transaction_failed' THEN visitor_id END) as failed_visitor_id,
    MIN(CASE WHEN transaction_action='transaction_completed' THEN visitor_id END) as completed_visitor_id  
  FROM exercise
  GROUP BY
    visitor_id,
    visit_num,
    transaction_type,
    CONVERT(visit_start_timestamp, DATE)
),

/* Perform multi-level aggregation of derived "visits" table. */
aggregations (transaction_date,transaction_type,visitors_total,visitors_started,visitors_failed,visitors_completed,visits_total,visits_started,visits_failed,visits_completed) as (
  SELECT
    transaction_date,
    CASE WHEN GROUPING(transaction_type) THEN'all' ELSE transaction_type END as transaction_type,
    COUNT(DISTINCT visitor_id) as visitors_total,
    COUNT(DISTINCT started_visitor_id) as visitors_started,
    COUNT(DISTINCT failed_visitor_id) as visitors_failed,
    COUNT(DISTINCT completed_visitor_id) as visitors_completed,
    COUNT(*) as visits_total,
    COUNT(started_visitor_id) as visits_started,
    COUNT(failed_visitor_id) as visits_failed,
    COUNT(completed_visitor_id) as visits_completed
  FROM visits 
  GROUP BY 
    transaction_date,
    transaction_type
    WITH ROLLUP
  HAVING GROUPING(transaction_date) = 0
)

/* Union the separate aggregation results for "visitors" and "visits". */
SELECT
  'visitors' as aggregation,
  transaction_type,  
  transaction_date,
  visitors_total as total,
  visitors_started as started,
  visitors_failed as failed,
  visitors_completed as completed
FROM aggregations

UNION ALL

SELECT
  'visits' as aggregation,
  transaction_type,  
  transaction_date,
  visits_total as total,
  visits_started as started,
  visits_failed as failed,
  visits_completed as completed
FROM aggregations

ORDER BY aggregation,FIELD(transaction_type,'credit','debit','bank'),transaction_date 
